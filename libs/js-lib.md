# 物理引擎



https://brm.io/matter-js/index.html

# 编辑器

A modern, simple and elegant WYSIWYG rich text editor.

https://github.com/raphaelcruzeiro/jquery-notebook

https://quilljs.com/

# UI 组件

纯JS组件库，可查看源代码

https://codyhouse.co/

React 组件库

https://galio.io/

https://www.iviewui.com/

https://taro-ui.aotu.io/#/

https://github.com/Authman2/Mosaic

https://www.csswand.dev/ CSS代码展示

[Vue Carousel 3D](https://wlada.github.io/vue-carousel-3d/)

https://varin6.github.io/Hover-Buttons/#

https://frontendsource.com/

# UI参考

https://hexometer.com/

https://www.minimamente.com/

https://dareful.com/

https://yourstack.com/

# 进度条

http://usablica.github.io/progress.js/

https://kimmobrunfeldt.github.io/progressbar.js/

```html
<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0zM13 10H7" />
</svg>
```

# 提示

Intro.js is a lightweight library for creating step-by-step customer onboarding

https://introjs.com

TOOLTIP & POPOVER  POSITIONING ENGINE

https://popper.js.org/

Tippy.js is the complete tooltip, popover, dropdown, and menu solution for the web, powered by Popper.

https://atomiks.github.io/tippyjs/

# 输入框

Placeholdem is a JavaScript plugin that animates placeholder carets on inputs and textareas. The placeholder value will incrementally delete on focus, and restore on blur.

http://placeholdem.jackrugile.com/



搜索功能

http://albburtsev.github.io/jquery.lookingfor/

# 表格

tiny editable jQuery Bootstrap spreadsheet

http://mindmup.github.io/editable-table/

包含大量常用电子表格功能,替代你的excel

https://mengshukeji.github.io/LuckysheetDocs/zh/



# 下拉菜单

The fast and capable dropdown library

http://github.hubspot.com/drop/docs/welcome/

# 代码

代码样式转换，它会自动识别代码，识别语言并高亮显示它们。

https://highlightjs.org/

# Canvas

Awesome interactive globes for the web

地球特效

http://planetaryjs.com/

# 数字

数字变化，CountUp.js is a dependency-free, lightweight JavaScript class that can be used to quickly create animations that display numerical data in a more interesting way.

http://inorganik.github.io/countUp.js/

# 图像

图片切割

https://github.com/fengyuanchen/cropperjs

# 图形

Obelisk.js这款JS引擎可以用来构建等距像素对象。它简单灵活的API接口方便开发者使用html5创建砖块、立方体、棱锥等物体

https://github.com/nosir/obelisk.js



网络类型图形

http://sigmajs.org/



SVG图形绘制

http://bonsaijs.org/



图表绘制

https://www.chartjs.org/



爬虫绘制程序

http://jmstriegel.github.io/jquery.spidergraph/



数据可视化

https://www.amcharts.com/



绘制可3d变换的2D图形

https://zzz.dog/



人工智能图片无损放大工具

https://www.uisdc.com/ai-image-enlarger





https://timqian.com/chart.xkcd/#introduction

# 滚动

https://github.com/kenwheeler/browserSwipe



一个非常完整的jquery插件。用于创建滚动动画。

http://scrollmagic.io/

# 动画

css动画特效，Just-add-water CSS animations

https://animate.style/

http://mobify.github.io/pikabu/

http://airbnb.io/lottie/#/README

https://popmotion.io/

https://webkul.github.io/micron/

http://leocs.me/jquery-drawsvg/

http://joanclaret.github.io/html5-canvas-animation/

```
background-image: linear-gradient(to right, #ff8177 0%, #ff867a 0%, #ff8c7f 21%, #f99185 52%, #cf556c 78%, #b12a5b 100%);
```

# 文件 & 文本

https://uppy.io/docs/

https://nicedoc.io/

https://github.com/kefranabg/readme-md-generator （md文档美化）

https://xtermjs.org/

https://bennettfeely.com/ztext/

# 颜色

https://webgradients.com/

https://calcolor.co/

# CSS框架

https://minicss.org/

https://colorswall.github.io/CSS-file-icons/

https://nostalgic-css.github.io/NES.css/

https://v3.getbuttercake.com/docs/

# 图库

https://remixicon.com/ 

https://pixelbuddha.net/

https://heroicons.com/

# 教学资源

https://www.flashcardsfordevelopers.com/

https://dss-lang.com/

https://dockerlabs.collabnix.com/

https://github.com/trimstray/nginx-admins-handbook

https://training.github.com/downloads/zh_CN/github-git-cheat-sheet/

https://scotch.io/

https://binarysearch.com/

# Lightbox

https://nextapps-de.github.io/spotlight/

# 语音

https://www.verby.co/

# 网站&博客

https://cssdeck.com/

https://www.smashingmagazine.com/

https://know-it-all.io/

https://developers.google.com/web/updates/2018/09/inside-browser-part1

https://www.kampsite.co/

# 开发资源

https://drawsql.app/

饿了么前端

https://github.com/ElemeFE

移动端JS判断手势方向

https://www.cnblogs.com/ranyonsue/p/7404173.html

免费资源

https://free-for.dev/#/?id=cms

# 游戏框架

Pharse 游戏框架
http://phaser.io/learn



# 作图

https://isoflow.io/# 物理引擎



https://brm.io/matter-js/index.html

# 编辑器

A modern, simple and elegant WYSIWYG rich text editor.

https://github.com/raphaelcruzeiro/jquery-notebook



https://quilljs.com/

# UI 组件

纯JS组件库，可查看源代码

https://codyhouse.co/

React 组件库

https://galio.io/

https://www.iviewui.com/

https://taro-ui.aotu.io/#/

https://github.com/Authman2/Mosaic

https://www.csswand.dev/ CSS代码展示

[Vue Carousel 3D](https://wlada.github.io/vue-carousel-3d/)

https://varin6.github.io/Hover-Buttons/#

# 进度条

http://usablica.github.io/progress.js/

https://kimmobrunfeldt.github.io/progressbar.js/



# 提示

Intro.js is a lightweight library for creating step-by-step customer onboarding

https://introjs.com

TOOLTIP & POPOVER  POSITIONING ENGINE

https://popper.js.org/

Tippy.js is the complete tooltip, popover, dropdown, and menu solution for the web, powered by Popper.

https://atomiks.github.io/tippyjs/

# 输入框

Placeholdem is a JavaScript plugin that animates placeholder carets on inputs and textareas. The placeholder value will incrementally delete on focus, and restore on blur.

http://placeholdem.jackrugile.com/



搜索功能

http://albburtsev.github.io/jquery.lookingfor/

# 表格

tiny editable jQuery Bootstrap spreadsheet

http://mindmup.github.io/editable-table/



# 下拉菜单

The fast and capable dropdown library

http://github.hubspot.com/drop/docs/welcome/

# 代码

代码样式转换，它会自动识别代码，识别语言并高亮显示它们。

https://highlightjs.org/

# Canvas

Awesome interactive globes for the web

地球特效

http://planetaryjs.com/

# 数字

数字变化，CountUp.js is a dependency-free, lightweight JavaScript class that can be used to quickly create animations that display numerical data in a more interesting way.

http://inorganik.github.io/countUp.js/

# 图像

图片切割

https://github.com/fengyuanchen/cropperjs

# 图形

Obelisk.js这款JS引擎可以用来构建等距像素对象。它简单灵活的API接口方便开发者使用html5创建砖块、立方体、棱锥等物体

https://github.com/nosir/obelisk.js



网络类型图形

http://sigmajs.org/



SVG图形绘制

http://bonsaijs.org/



图表绘制

https://www.chartjs.org/



爬虫绘制程序

http://jmstriegel.github.io/jquery.spidergraph/



数据可视化

https://www.amcharts.com/



绘制可3d变换的2D图形

https://zzz.dog/



人工智能图片无损放大工具

https://www.uisdc.com/ai-image-enlarger





https://timqian.com/chart.xkcd/#introduction

# 滚动

https://github.com/kenwheeler/browserSwipe



一个非常完整的jquery插件。用于创建滚动动画。

http://scrollmagic.io/

# 动画

css动画特效，Just-add-water CSS animations

https://animate.style/

http://mobify.github.io/pikabu/

http://airbnb.io/lottie/#/README

https://popmotion.io/

https://webkul.github.io/micron/

http://leocs.me/jquery-drawsvg/

http://joanclaret.github.io/html5-canvas-animation/

```
background-image: linear-gradient(to right, #ff8177 0%, #ff867a 0%, #ff8c7f 21%, #f99185 52%, #cf556c 78%, #b12a5b 100%);
```

# 文件 & 文本

https://uppy.io/docs/

https://nicedoc.io/

https://github.com/kefranabg/readme-md-generator （md文档美化）

https://xtermjs.org/

# 颜色

https://webgradients.com/

# CSS框架

https://minicss.org/

https://colorswall.github.io/CSS-file-icons/

# 图库

https://remixicon.com/ 

https://pixelbuddha.net/

# 教育

https://www.flashcardsfordevelopers.com/

https://dss-lang.com/

https://dockerlabs.collabnix.com/

https://github.com/trimstray/nginx-admins-handbook

https://training.github.com/downloads/zh_CN/github-git-cheat-sheet/

# Lightbox

https://nextapps-de.github.io/spotlight/

# 语音

https://www.verby.co/

# 网站&博客

https://cssdeck.com/

https://www.smashingmagazine.com/

https://know-it-all.io/

https://developers.google.com/web/updates/2018/09/inside-browser-part1

https://codersblock.com/

# 开发

https://filecoin.io/zh-cn/


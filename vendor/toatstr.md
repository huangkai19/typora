# Toastr插件

定义回调

```javascript
/**
 * 显示提示
 * @param {*} type 提示类型:info-普通提示；success-成功提示；error-错误提示
 * @param {*} msg 
 */
function toastr_show(type, msg) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "progressBar": false,
        "positionClass": "toast-bottom-center",
        "onclick": null,
        "showDuration": "20",
        "hideDuration": "100",
        "timeOut": "1000",
        "extendedTimeOut": "100",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    
    toastr[type] && toastr[type]("" , msg);
}
```



成功提示

```javascript
toastr_show('success' , '成功');
```



失败提示

```javascript
toastr_show('error' , '失败');
```


# 百度地图

## 注册

1. 注册百度开发者帐号
2. 添加地图应用，包含引用地图的应用域名，应用类型（服务端应用和浏览器端的apk是不同的）



## 开发

引入地图

```javascript
<script src="//api.map.baidu.com/api?v=2.0&ak=${baiduApk}"></script>
```

其中：baiduApk为浏览器端的apk



初始化地图

```javascript
var map = new BMap.Map('mapContainer'); // 创建Map实例
map.centerAndZoom("中山市" , 12);
map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
```

参考地址：http://lbsyun.baidu.com/jsdemo.htm#aCreateMap



## 应用实例

### 1、关键字搜索定位

大致效果：地图上有个输入框，输入关键字后显示搜索结果列表，点击某一条记录，在地图上定位并显示标注

示例化一个监听输入的对象，input是输入框的ID属性，location是地图对象

```javascript
var ac = new BMap.Autocomplete({"input": "address_search" ,"location" : map });
var myValue, localSearch, posInfo;
ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
    var _value = e.item.value;
    //生成具体的地址
    myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
    //设置
    setPlace();
});

//获取第一个智能搜索的结果
function handleSearchComplete(){
    var pp = localSearch.getResults().getPoi(0).point;
    posInfo.lat = pp.lat;
    posInfo.lng = pp.lng;
    map.centerAndZoom(pp, 18);
    var marker = new BMap.Marker(pp,{ enableDragging: true });
    marker.addEventListener("dragend", function(e){
        //拖拽完成后的监听
        posInfo.lat = e.point.lat;
        posInfo.lng = e.point.lng;
    })
    map.addOverlay(marker);
}

function setPlace(){
    map.clearOverlays();    //清除地图上所有覆盖物
    localSearch = new BMap.LocalSearch(map, { //智能搜索
        onSearchComplete: handleSearchComplete
    });
    localSearch.search(myValue);
}
```


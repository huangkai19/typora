# LayerUI使用

## 官方

https://www.layui.com/

文档：https://www.layui.com/doc/



## 应用实例

### 弹窗提示

只有一个按钮提示

```javascript
function layerTip(msg, btnText) {
    layer.confirm(msg, { title:'提示' ,close:0, btn: [btnText||'确定'] },
        function(index){//确定事件
            layer.close(index);
    });
}
```

两个按钮确认框

```javascript
function layerComfirm(msg , callback) {
    layer.confirm(msg, { title:'提示', btn: ['确定','取消']},
        function(index){//确定事件
            layer.close(index);
        if(typeof(callback) === "function"){
            callback("ok");
        }
    });
}
```

### 加载效果

加载效果

```javascript
layer.load(2);
```

关闭加载

```javascript
layer.closeAll('loading');
```


# 前端开发笔记

## 介绍

现在的前端开发技术层出不穷，对于基础的知识还是要加深巩固

### 入门知识

*介绍前端主要技术的基础知识点*

- HTML5
- CSS3
- Javascript

### 前端框架技术

*主流前端框架应用技术*

- typescript
- [Vue.js](vue/intro.md)
- React.js
- JQuery.js
- Node.js

### 图形化技术

*图形绘制相关技术*

- Canvas
- SVG
- Three.js
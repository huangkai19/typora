/*
Navicat MySQL Data Transfer

Source Server         : local_db
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : db_qy

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2021-02-08 18:26:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for qy_content
-- ----------------------------
DROP TABLE IF EXISTS `qy_content`;
CREATE TABLE `qy_content` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `ctitle` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `ccateid` int(4) NOT NULL DEFAULT '0',
  `ctime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_content
-- ----------------------------
INSERT INTO `qy_content` VALUES ('1', '内容1', '好的', '0', '2021-02-08 14:06:52');
INSERT INTO `qy_content` VALUES ('2', '内容2', '真好', '0', '2021-02-08 14:07:14');

-- ----------------------------
-- Table structure for qy_content_data
-- ----------------------------
DROP TABLE IF EXISTS `qy_content_data`;
CREATE TABLE `qy_content_data` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(4) NOT NULL COMMENT '0',
  `field_name` varchar(50) DEFAULT NULL,
  `field_value` varchar(255) DEFAULT NULL,
  `field_int` decimal(11,0) DEFAULT NULL COMMENT '0',
  `cd_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_content_data
-- ----------------------------
INSERT INTO `qy_content_data` VALUES ('1', '1', 'info_from', '本地新闻', null, 'article');
INSERT INTO `qy_content_data` VALUES ('2', '1', 'info_num', null, '1', 'article');

-- ----------------------------
-- Table structure for qy_cont_mod
-- ----------------------------
DROP TABLE IF EXISTS `qy_cont_mod`;
CREATE TABLE `qy_cont_mod` (
  `cm_id` int(4) NOT NULL AUTO_INCREMENT,
  `cm_name` varchar(255) NOT NULL,
  `cm_flag` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_cont_mod
-- ----------------------------

-- ----------------------------
-- Table structure for qy_func_menu
-- ----------------------------
DROP TABLE IF EXISTS `qy_func_menu`;
CREATE TABLE `qy_func_menu` (
  `fmenu_id` int(4) NOT NULL AUTO_INCREMENT,
  `fmenu_name` varchar(255) NOT NULL,
  `fmenu_pid` int(4) NOT NULL DEFAULT '0',
  `site_id` int(4) NOT NULL DEFAULT '0',
  `fmenu_path` varchar(255) DEFAULT NULL,
  `fmenu_fmid` int(4) NOT NULL,
  `fmenu_action` varchar(50) NOT NULL,
  PRIMARY KEY (`fmenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_func_menu
-- ----------------------------
INSERT INTO `qy_func_menu` VALUES ('6', '新闻管理', '0', '8', '', '0', '');
INSERT INTO `qy_func_menu` VALUES ('7', '公司动态', '6', '8', 'm=articles&a=list', '1', 'list');
INSERT INTO `qy_func_menu` VALUES ('8', '行业动态', '6', '8', 'm=articles&a=list', '1', 'list');
INSERT INTO `qy_func_menu` VALUES ('9', '产品管理', '0', '8', 'm=product', '0', '');
INSERT INTO `qy_func_menu` VALUES ('10', '产品分类', '9', '8', 'm=procate&a=list', '10', 'list');
INSERT INTO `qy_func_menu` VALUES ('11', '产品管理', '9', '8', 'm=product&a=list', '2', 'list');
INSERT INTO `qy_func_menu` VALUES ('12', '案例方案', '0', '8', '', '0', '');
INSERT INTO `qy_func_menu` VALUES ('13', '案例', '12', '8', 'm=articles&a=list', '1', 'list');
INSERT INTO `qy_func_menu` VALUES ('14', '方案', '12', '8', 'm=articles&a=list', '1', 'list');
INSERT INTO `qy_func_menu` VALUES ('15', '企业信息', '0', '8', '', '0', '');
INSERT INTO `qy_func_menu` VALUES ('16', '公司介绍', '15', '8', 'm=articles&a=add', '1', 'add');
INSERT INTO `qy_func_menu` VALUES ('17', '荣誉证书', '15', '8', 'm=photo', '0', '');
INSERT INTO `qy_func_menu` VALUES ('18', '公司参观', '15', '8', 'm=photo', '0', '');

-- ----------------------------
-- Table structure for qy_func_mod
-- ----------------------------
DROP TABLE IF EXISTS `qy_func_mod`;
CREATE TABLE `qy_func_mod` (
  `fm_id` int(4) NOT NULL AUTO_INCREMENT,
  `fm_name` varchar(255) NOT NULL,
  `fm_path` varchar(255) NOT NULL,
  `fm_pid` int(4) DEFAULT '0',
  `fm_desc` varchar(255) DEFAULT NULL,
  `fm_options` longtext,
  PRIMARY KEY (`fm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_func_mod
-- ----------------------------
INSERT INTO `qy_func_mod` VALUES ('1', '文章管理', 'articles', '0', '可以发布新闻咨询、单独的页面内容等等', null);
INSERT INTO `qy_func_mod` VALUES ('2', '产品', 'product', '0', '可以发布产品相关信息', null);
INSERT INTO `qy_func_mod` VALUES ('7', '人事资源', 'recruitment', '0', '建立招聘政策，人事招聘信息', null);
INSERT INTO `qy_func_mod` VALUES ('8', '自定义联系方式', 'contact', '0', '自定义联系方式，地址，地图，电话，邮箱等等', null);
INSERT INTO `qy_func_mod` VALUES ('9', '留言管理', 'guest', '0', '收集用户留言信息，后台支持回复', null);
INSERT INTO `qy_func_mod` VALUES ('10', '产品分类', 'procate', '0', '编辑产品分类，可以新增分类封面', null);
INSERT INTO `qy_func_mod` VALUES ('11', '图片管理', 'photo', '0', '图片资源管理发布', null);
INSERT INTO `qy_func_mod` VALUES ('12', '图片分类管理', 'photocate', '0', '图片分类设置', null);

-- ----------------------------
-- Table structure for qy_nav
-- ----------------------------
DROP TABLE IF EXISTS `qy_nav`;
CREATE TABLE `qy_nav` (
  `nav_id` int(4) NOT NULL AUTO_INCREMENT,
  `nav_title` varchar(50) NOT NULL COMMENT '导航标题',
  `nav_pid` int(4) DEFAULT '0',
  `nav_url` varchar(255) DEFAULT NULL COMMENT '导航链接',
  PRIMARY KEY (`nav_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_nav
-- ----------------------------

-- ----------------------------
-- Table structure for qy_sites
-- ----------------------------
DROP TABLE IF EXISTS `qy_sites`;
CREATE TABLE `qy_sites` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) NOT NULL COMMENT '网站名称',
  `site_url` varchar(255) NOT NULL,
  `site_title` varchar(255) NOT NULL COMMENT '网站标题',
  `site_keyword` varchar(255) DEFAULT NULL COMMENT '网站关键字',
  `site_desc` varchar(255) DEFAULT NULL COMMENT '网站描述',
  `site_email` varchar(255) DEFAULT NULL COMMENT '邮箱地址',
  `site_isclose` tinyint(2) DEFAULT '0' COMMENT '是否关闭:0-否,1-是',
  `site_closetime` int(11) DEFAULT '0' COMMENT '关闭时间',
  `site_langs` varchar(255) NOT NULL COMMENT '网站语言',
  `site_isdemo` tinyint(2) DEFAULT '0' COMMENT '是否演示: 0-否,1-是',
  `site_outtime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `site_outmsg` varchar(255) DEFAULT NULL,
  `site_current` int(3) DEFAULT '0' COMMENT '当前编辑网站',
  `site_otherdb` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_sites
-- ----------------------------
INSERT INTO `qy_sites` VALUES ('8', '三只小猪', 'http://localhost:83', '三只小猪', '三只小猪猪的喔', '三只小猪猪的喔', '279639439@qq.com', '0', '0', 'zh', '0', '2020-08-06 12:03:16', '', '1', '0');

-- ----------------------------
-- Table structure for qy_sites_db
-- ----------------------------
DROP TABLE IF EXISTS `qy_sites_db`;
CREATE TABLE `qy_sites_db` (
  `db_id` int(3) NOT NULL AUTO_INCREMENT,
  `db_name` varchar(255) NOT NULL,
  `db_host` varchar(255) NOT NULL,
  `db_root` varchar(255) NOT NULL,
  `db_pwd` varchar(255) NOT NULL,
  `db_siteid` int(4) DEFAULT NULL,
  `db_adminurl` varchar(255) NOT NULL,
  PRIMARY KEY (`db_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_sites_db
-- ----------------------------

-- ----------------------------
-- Table structure for qy_user
-- ----------------------------
DROP TABLE IF EXISTS `qy_user`;
CREATE TABLE `qy_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `user_pwd` varchar(255) NOT NULL,
  `user_groupid` int(4) DEFAULT NULL,
  `user_siteid` int(4) DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_user
-- ----------------------------
INSERT INTO `qy_user` VALUES ('2', 'user1e', 'e10adc3949ba59abbe56e057f20f883e', '1', '8');
INSERT INTO `qy_user` VALUES ('3', 'user2', 'e10adc3949ba59abbe56e057f20f883e', '0', '8');
INSERT INTO `qy_user` VALUES ('4', 'admin', 'af974cf3ae8a5bf92832a864766f5b6c', '0', '0');

-- ----------------------------
-- Table structure for qy_user_group
-- ----------------------------
DROP TABLE IF EXISTS `qy_user_group`;
CREATE TABLE `qy_user_group` (
  `ugroup_id` int(4) NOT NULL AUTO_INCREMENT,
  `ugroup_name` varchar(20) DEFAULT NULL,
  `ugroup_siteid` int(4) DEFAULT '0',
  PRIMARY KEY (`ugroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qy_user_group
-- ----------------------------
INSERT INTO `qy_user_group` VALUES ('1', '网页编辑', '8');

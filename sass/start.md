# SASS入门

## 简介

它是一门CSS预编译技术，通过特定的语法规则来编写CSS样式。

它的代码保存在`.scss`文件中，浏览器不能解析`.scss`，只能通过编译工具将SASS代码编译成`.css`文件



## 开发环境配置

开发工具： Vs Code

开发插件：Live Sass Compiler（实时编译SASS），Live Server（本地服务器，静听静态资源变化）

开启的方法：

1. 在Vscode开发工具的底部，有个 “Watch Sass”的按钮，点击它，就会自动给监听所有.scss文件并编译成.css
2. 在HTML页面，右键“Open with Live Server”，开启一个本地服务器，自动打开浏览器窗口


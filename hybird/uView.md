# uView

## 官方资源

https://www.uviewui.com



## 组件应用

### 图标

- 行内元素
- 根据name来设置图标，可以是网络资源（url地址）
- 根据size设置大小
- 根据color设置颜色

```vue
<u-icon name="arrow-right" color="#2979ff" size="28"></u-icon>
```

### 复选框

- @change 绑定变更事件
- v-model 绑定数据
- shape 绑定原型

```vue
<u-checkbox shape="circle" @change="checkboxChange">全选</u-checkbox>
```

### 步进器

- @change 绑定变更事件
- v-model 绑定数据

```vue
<u-number-box v-model="value" @change="valChange"></u-number-box>
```

### 自定义导航

在导航中无自定义布局

```vue
<u-navbar back-text="返回" title="剑未配妥，出门已是江湖"></u-navbar>
```

在导航中有自定义布局，不设置`back-text`和`title`

```vue
<u-navbar>
    <view class="col_search">
        <!--搜索-->
        <u-search placeholder="请输入关键字" v-model="keyword" :show-action="false"></u-search>
	</view>
</u-navbar>
```

### 搜索框

```vue
<u-search placeholder="请输入关键字" v-model="keyword" :show-action="false"></u-search>
```

### tabs选项卡

```vue
<u-tabs :list="list" :is-scroll="false" :current="current" @change="change"></u-tabs>
```

### 输入框

```vue
<u-input v-model="value" :type="type" :border="border" :height="height" :auto-height="autoHeight" />
```

### 线条

```vue
<u-line color="red"></u-line>
```

### 评分

```vue
<u-rate active-color="#FFD21F" :count="5" v-model="rateVal"></u-rate>
```


# Uni-app 踩坑日志

## 安装初始化项目

### 1. 官方

下载：https://www.dcloud.io/hbuilderx.html



### 2. 配置

#### 2.1安装SASS，LESS，内置浏览器插件

通过插件市场安装

#### 2.2 配置微信开发者工具

工具 -> 设置 -> 运行设置

在开发者工具的设置， 设置 -> 安全设置，将服务端口开启。



## 分包

```javascript

    "subPackages": [{
        "root": "pagesA",
        "pages": [{
            "path": "list/list",
            "style": { ...}
        }]
    }, {
        "root": "pagesB",
        "pages": [{
            "path": "detail/detail",
            "style": { ...}
        }]
    }],
```



### 页面常用设置

位置： pages.json -> pages -> style

**页面标题**

"navigationBarTitleText": "标题"





## 第三方资源

### uView

https://www.uviewui.com






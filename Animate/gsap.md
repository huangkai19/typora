# GSAP

## 官网

https://greensock.com/get-started/



## 引入

```javascript
https://cdn.jsdelivr.net/npm/gsap@3.12.1/dist/gsap.min.js
```



## 使用

基础动画

```javascript
gsap.to(".box", { x: 200 })
```





## 应用场景

- 创建复杂动画

- 位移
- 位移 + 变幻
- 用于SVG
- 用于Canvas
# AniJS

## 官网资源

http://anijs.github.io/



## 使用方法

```html
<div data-anijs="if: click, do: flipInY, to: .container-box"></div>
```

自定义事件

```javascript
// Tip: avoid this ton of code using AniJS ;)

var element = $('#square');

// when mouseover execute the animation
element.mouseover(function(){
  
  // the animation starts
  element.toggleClass('bounce animated');
  
  // do something when animation ends
  element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e){
   
   // trick to execute the animation again
    $(e.target).removeClass('bounce animated');
  
  });
  
});
```



## 适用场景

- 效果类，隐藏、淡入淡出等
- 从侧边弹出等
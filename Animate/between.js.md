# Between.js

# 官网资源

github: https://github.com/sasha240100/between.js

site: https://between.js.org/



## 使用的方式

设置动画的起始状态，监听开始->变化过程->结束的状态

起始状态：

```javascript
// 简单的数字
(1, 10)

// 一个对象的多个属性
({x: 0, y: 0, r: 0}, {x: 400, y: 30, r: 90})

// 数组
([1, 5], [10, 10])
```



示例代码

```javascript
const element = document.querySelector('#move');
const state = document.querySelector('[data-state]');

window.onload = () => setTimeout(() => { // Wait for page loading
  new Between(0, 400).time(2000).on('update', v => {
    element.style.transform = `translateX(${v}px)`;
  }).on('start', () => {
    state.innerText = 'state: running';
  }).on('complete', () => {
    state.innerText = 'state: complete';
  });
}, 2000);

```



## 场景

- 大屏中 数字变换
- 元素位置移动
- 元素形状变化
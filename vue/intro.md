# 什么是Vue.js

## 简介

- 一套响应式UI框架，通过数据绑定的方式来实现基本的UI交互
- 支持组件式开发，提高代码复用率
- 完善生态系统，可结合第三方库实现不同的功能



## 诞生

2015年，发布了v1.0版本

2016年，发布了v2.0版本



## 核心概念

### 双向数据绑定

采用简洁的模板语法将数据声明式渲染更新DOM，Vue提供对View和ViewModel的双向数据绑定，一方更新了触发另外一方的更新。ViewModel是一个Vue的实例对象，它负责链接Model和View，保证视图和数据一致。



### 指令

通过表达式将某些相应行为应用到DOM上



### 组件化

用于扩展UI，封装成可复用的代码。在Vue中，组件支持设置层级关系，父组件与子组件可以相互通信



## 资源

https://cn.vuejs.org/



## 安装

### 直接引入CDN地址

官网提供了一个CDN的地址，通过`<script>`标签引入Vue.js库

```javascript
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
```

HTML代码：

```html
<div id="app">
    {{msg}}
</div>
```

JS代码：

```javascript
new Vue({
    el: '#app',
    data: {
        msg: 'Hello'
    }
});
```



### npm

环境要求：node，npm

```javascript
//全局安装vue-cli脚手架
npm install --g @vue/cli-init

//使用webpack模板创建项目
vue init webpack vdemo1

//启动项目
cd vdemo1
npm run dev

//访问项目
浏览器访问 http://localhost:8080
```


# Vue指令

Vue指令是带有`v-`前缀的特殊属性，指令的值是一个Javascript表达式

它的作用是当表达式的计算结果，产生的行为变化应用到DOM上

# 语法

### 基本入门

通过一个简单的案例来了解Vue指令的使用，隐藏和显示一些元素可以通过`v-if`指令来完成

`v-if`的值为true显示元素，值为false则隐藏元素

HTML

```html
<div id="app">
    <p v-if="pHide">隐藏的元素</p>
    <p v-if="pShow">显示的元素</p>
</div>
```

JS

```javascript
new Vue({
    el: '#app',
    data: {
        pShow: true,
        pHide: false
    }
});
```

### 修饰符

通过修饰符来为Vue指令扩展一些行为，比如在指令后面加上`.stop`

HTML

```html
<button v-on:click.stop="bindClick">点击一下</button>
```

JS

```javascript
new Vue({
    el: '#app',
    methods: {
        bindClick: function() {
            alert(1);
        }
    }
});
```



## 具体指令介绍



